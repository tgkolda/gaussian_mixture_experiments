function CholSigma = checkPSD(obj, Sigma)


    if obj.CovType == "full"
        for j = size(Sigma, 3):-1:1
            % Make sure Sigma is a valid covariance matrix
            % check for positive definite
            [CholSigma(:,:,j), err] = cholcov(Sigma(:,:,j));
            if err ~= 0
                error(message('stats:gmdistribution:BadCov'));
            end
        end
    elseif any(Sigma<0)
        error(message('stats:gmdistribution:BadDiagCov'));
    else
        CholSigma = sqrt(Sigma);
    end
    
end
