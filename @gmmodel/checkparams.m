function [checkpassed, err] = checkparams(obj, n, r, Shared, Type, error)
%CHECKPARAMS Checks gmmodel has given parameters

tests = [(obj.NumVariables     == n);...
         (obj.NumComponents    == r);...
         (obj.SharedCovariance == Shared);...
         (obj.CovarianceType   == Type)];

checkpassed = all(tests);

err = find(~tests, 1);

if ~checkpassed && nargout > 4
    switch err
        case 1
            error('NumVariables is different')
        case 2
            error('NumComponents is different')
        case 3
            error('SharedCovariance is different')
        case 4
            error('CovarianceType is different')
    end
end

end

