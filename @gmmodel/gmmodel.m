classdef gmmodel < gmdistribution
%GMMODEL Gaussian mixture distribution class with added functionality.
%   An object of the GMMODEL class defines a Gaussian mixture distribution,
%   which is a multivariate distribution that consists of a mixture of one 
%   or more multivariate Gaussian distribution components.
%   The class GMMODEL is a subclass of the GMDISTRIBUTION class that is
%   distributed in MATLAB, with added functionalities, such as
%      - New covariance type options: spherical or isotropic, when the
%        covariances are multiples of the identity matrix.
%      - Cholesky decomposition of the covariances.
%      - Functions to convert model to vector and update model from data
%        of a vector.
%   
%   To create a Gaussian mixture distribution by specifying the
%   distribution parameters, use the GMMODEL constructor. The constructor
%   can also be used to convert GMDISTRIBUTION objects to GMMODEL
%
%   GMMODEL properties:
%   A Gaussian mixture distribution with K components, in D dimensions, has
%   the following properties:
%      NumVariables          - Number of variables(dimensions).
%      DistributionName      - Name of the distribution.
%      NumComponents         - Number of mixture components.
%      ComponentProportion   - Mixing proportion of each component.
%      CovarianceType        - Type of the component covariance matrices.
%      SharedCovariance      - A logical indicating whether all the component covariance are the same.
%      mu                    - Matrix of component means.
%      Sigma                 - Component covariance
%      CholSigma             - Cholesky decomposition of Component covariance
%
%   GMMODEL methods:
%      cdf            - CDF for the Gaussian mixture distribution.
%      cluster        - Cluster data for Gaussian mixture distribution.
%      mahal          - Mahalanobis distance to component means.
%      gmmodel        - Create a Gaussian mixture model.
%      pdf            - PDF for Gaussian mixture distribution.
%      log_pdf        - Logarithm of PDF for Gaussian mixture distribution.
%      posterior	  - Posterior probabilities of components.
%      random         - Random numbers from Gaussian mixture distribution
%      tovec          - Convert model to vector
%      update         - Update model using data from vector
%
%
%   See also GMDISTRIBUTION, FITGMDIST

%   Based on GMDISTRIBUTION,  Copyright 2007-2018 The MathWorks, Inc.

    properties(GetAccess='protected', SetAccess='protected')
        CholSigma_ = [];       % Covariance
    end
    
    properties(GetAccess='public', SetAccess='protected', Dependent=true) 
        CholSigma      % Covariance
    end

    methods
        function obj = gmmodel(mu, Sigma, p, ischol)
        %GMMODEL Create a Gaussian mixture model.
        %   GM = GMMODEL(GMD), where GMD is a GMDISTRIBUTION, converts
        %   GMD to a GMMODEL object.
        %
        %   GM = GMMODEL(MU, SIGMA, P, ischol) creates a distribution
        %   consisting of a mixture of multivariate Gaussian components,
        %   given values for the components' distribution parameters. 
        %
        %   The number of components and the dimension of the distribution are
        %   implicitly defined by the sizes of the inputs MU, SIGMA, and P.
        %
        %   MU is K-by-D matrix specifying the mean of each component, where K
        %   is the number of components, and D is the number of dimensions.  MU(J,:)
        %   is the mean of component J.
        %
        %   SIGMA specifies the covariance matrix of each component.  SIGMA is one
        %   of the following:
        %
        %      * A D-by-D-by-K array if there are no restrictions on the form of the
        %        covariance matrices.  In this case, SIGMA(:,:,J) is the covariance
        %        matrix of component J.
        %      * A 1-by-D-by-K array if the covariance matrices are restricted to be
        %        diagonal, but not restricted to be same across components.  In this
        %        case, SIGMA(:,:,J) contains the diagonal elements of the covariance
        %        matrix of component J.
        %      * A 1-by-1-by-K array if the covariance matrices are restricted to be
        %        spherical, but not restricted to be same across components. In this
        %        case, the covariance matrix of component J is the identity matrix,
        %        multiplied by SIGMA(:,:,J).
        %      * A D-by-D matrix if the covariance matrices are restricted to be the
        %        same across components, but not restricted to be diagonal.  In this
        %        case, SIGMA is the common covariance matrix.
        %      * A 1-by-D vector if the covariance matrices are restricted to be
        %        diagonal and the same across components. In this case, SIGMA contains
        %        the diagonal elements of the common covariance matrix.
        %      * A scalar if the covariance matrices are restricted to be spherical
        %        and the same across components. In this case, the common covariance 
        %        matrix is an identity matrix multiplied by SIGMA.
        %
        %   P is 1-by-K vector specifying the mixing proportions of each component.  
        %   If P does not sum to 1, GMMODEL normalizes it.  The default is equal
        %   proportions if P is not given.
        %
        %   The inputs MU, SIGMA, and P are stored in the mu, Sigma, and PComponents
        %   properties, respectively, of GM.
        %
        %   Example:  Create a 2-component Gaussian mixture model.
        %
        %            mu = [1 2;-3 -5];
        %            Sigma = cat(3,[2 0; 0 .5],[1 0; 0 1]);
        %            mixp = ones(1,2)/2;
        %            gm = gmdistribution(mu,Sigma,mixp);
        %
        %   See also GMDISTRIBUTION, FITGMDIST.
        
            if nargin==0
                return;
            end

            % Copy constructor
            if isa(mu, 'gmdistribution')
                obj = gmmodel(mu.mu, mu.Sigma, mu.PComponents, false);
                return
            end

            if iscell(mu)
                obj = gmmodel(mu{:});
                return
            end
            
            if nargin < 2
                error(message('stats:gmdistribution:TooFewInputs'));
            end
            if ~ismatrix(mu) || ~isnumeric(mu)
                error(message('stats:gmdistribution:BadMu'));
            end

            [k,d] = size(mu);
            if nargin < 3 || isempty(p)
                p = ones(1,k);
            elseif ~isvector(p) || length(p) ~= k
                error(message('stats:gmdistribution:MisshapedMuP'));
            elseif any(p < 0)
                error(message('stats:gmdistribution:InvalidP'));
                      
            elseif size(p,1) ~= 1
                p = p'; % make it a row vector
            end
            
            if nargin<4
                ischol = false;
            end

            p = p/sum(p);

            [d1,d2,k2] = size(Sigma);
            
            switch k2
                case 1
                    obj.SharedCov = true;
                case k
                    obj.SharedCov = false;
                otherwise
                    error(message('stats:gmdistribution:MisshapedMuCov'));
            end
            
            if d1 == 1 % diagonal covariance          
                if d2 == 1
                    obj.CovType = "spherical";
                elseif d2 == d
                    obj.CovType = "diagonal";
                else
                    error(message('stats:gmdistribution:MisshapedDiagCov'));
                end
            elseif d1 == d && d2 == d
                obj.CovType = "full";    
            else
                error(message('stats:gmdistribution:MisshapedCov'));
            end
           
            obj.NDimensions = d;
            obj.NComponents = k;
            
            type = superiorfloat(mu,Sigma,p);
            obj.PComponents = cast(p,type);
            obj.mu = cast(mu,type);
            
            if ischol
                obj.CholSigma = cast(Sigma, type); 
            else
                Sigma = cast(Sigma, type);
                obj.CholSigma_ = obj.checkPSD(Sigma);
                obj.Sigma = Sigma;
            end
            
            
        end % constructor
               
        function CholSigma = get.CholSigma(obj)
            if isempty(obj.CholSigma_)
                if obj.CovType == "full"
                    for j = size(obj.Sigma, 3):-1:1
                        CholSigma(:,:,j) = cholcov(obj.Sigma(:,:,j));
                    end
                else
                    CholSigma = sqrt(obj.Sigma);
                end
            else
                CholSigma = obj.CholSigma_;
            end
                
        end
        
        function obj = set.CholSigma(obj, CholSigma)
            obj.CholSigma_ = CholSigma;
            if obj.CovType == "full"
                obj.Sigma = pagemtimes(CholSigma, 'ctranspose',...
                                       CholSigma, 'none' );
            else
                obj.Sigma = CholSigma.^2;
            end
        end

    end
 
end
