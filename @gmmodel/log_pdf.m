function log_lh = log_pdf(M, X)
% LOG_PDF logarithm of PDF for a Gaussian mixture distribution.
%
%  [W,MU] = GMM_EM_SIGMA(X,SIGMA,R) computes the means of the Gaussian
%  mixture model with known covariance. Here X is an N x P matrix of P
%  observations, SIGMA is the N x N covariance matrix, and R is the number
%  of components. It returns W, the M weights of the GMM, and MU, an N x M
%  matrix of the component means. 
%
%  **NOTE**: The vector and matrix orientiations are different than those
%  expected by MATLAB's GMM functionality.

%% Preprocessing

Mu = M.mu;
CholSigma = M.CholSigma;
p = M.PComponents;
r = M.NumComponents;

[N, n] = size(X);
assert(n == size(Mu,2));

if M.SharedCovariance
    if M.CovarianceType == "full"
        XCi = X / CholSigma;
        MuCi = Mu / CholSigma;
        logdet = log_det(CholSigma);
    else
        XCi = X ./ CholSigma;
        MuCi = Mu ./ CholSigma;
        if M.CovarianceType == "diagonal"
            logdet = sum(log(CholSigma));
        else
            logdet = n*log(CholSigma);
        end
    end
    nXCi = vecnorm(XCi, 2, 2).^2;
    nMuCi = vecnorm(MuCi').^2;
    P_log = -(nXCi + nMuCi - 2 * XCi*MuCi') / 2;    
else
    logdet = zeros(1, r);
    P_log = zeros(N, r);
    for k = 1:r
        if M.CovarianceType == "full"
            V = (X - Mu(k, :)) / CholSigma(:,:,k);
            logdet(k) = log_det(CholSigma(:,:,k));
        else
            V = (X - Mu(k, :)) ./ CholSigma(:,:,k);
            if M.CovarianceType == "diagonal"
                logdet(k) = sum(log(abs(CholSigma(:,:,k))));
            else
                logdet(k) = n*log(abs(CholSigma(k)));
            end
        end
        P_log(:, k) = -vecnorm(V, 2, 2).^2 / 2;
    end
end

P_log = P_log + (log(p) - log(2*pi)*n/2 - logdet);
P_log_m = max(P_log, [], 2);
P_log_s = P_log - P_log_m;
log_lh = P_log_m + log(sum(exp(P_log_s), 2));

end

function logdet = log_det(Sigma)

if istriu(Sigma) || istril(Sigma)
    logdet = sum(log(abs(diag(Sigma))));
else
    logdet = sum(log(svd(Sigma)));
end

end