function lh = pdf(M, X)
% LOG_PDF logarithm of PDF for a Gaussian mixture distribution.
%
%  [W,MU] = GMM_EM_SIGMA(X,SIGMA,R) computes the means of the Gaussian
%  mixture model with known covariance. Here X is an N x P matrix of P
%  observations, SIGMA is the N x N covariance matrix, and R is the number
%  of components. It returns W, the M weights of the GMM, and MU, an N x M
%  matrix of the component means. 
%
%  **NOTE**: The vector and matrix orientiations are different than those
%  expected by MATLAB's GMM functionality.

%% Preprocessing

lh = exp(log_pdf(M, X));