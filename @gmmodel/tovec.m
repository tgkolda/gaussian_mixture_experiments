function x = tovec(M, ischol)
%TOVEC Convert symktensor to vector representation.
%
%   V = TOVEC(S) converts a symktensor to a vector. It stacks the LAMBDA
%   vector on top of a vectorized version of the matrix X.
%
%   V = TOVEC(S,TRUE) just returns a vectorized version of the matrix
%   X. It requires LAMBDA=1.
%
%Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>

if nargin < 2; ischol = false; end

if ~ischol
    x = [M.mu(:);M.Sigma(:);M.PComponents(:)];
else
    x = [M.mu(:);M.CholSigma(:);M.PComponents(:)];
end

end
