function M = update(M, varargin)
%UPDATE Convert vector to gmmodel.
%
%   M = UPDATE(M,V) overwrites some or all of the information in the
%   symktensor M with data from the vector V. Assume M is a symktensor of
%   dimension N with R components. If V is a vector of length (N+1)*R, then
%   the first R entries overwrite the weight vector M.lambda and the
%   remainder overwrites the factor matrix M.U. If V is a vector of length
%   N*R, then that data overwrites the factor matrix M.U and the M.lambda
%   is unchanged. This function is typically used in the context of 
%   optimization methods.
%
%Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>

if nargin == 2 || nargin == 4
    ischol = false;
else
    ischol = varargin{nargin-1};
end

if nargin < 4
    v = varargin{1};

    [mu, Sigma, p] = M.vec2params(v);
else
    param = {M.mu, M.Sigma, M.PComponents};
    
    for i=1:3
        if ~isempty(varargin{i}) && any(size(param{i}) ~= size(varargin{i}))
            switch i
            case 1
                error('The sizes of mu do not match')
            case 2
                error('The sizes of Sigma do not match')
            case 3
                error('The sizes of p do not match')
            end
        end
    end
    
    [mu, Sigma, p] = varargin{1:3};
end

M.mu = mu;
if isempty(Sigma)
elseif ischol
    M.CholSigma = Sigma;
else
    M.CholSigma_ = M.checkPSD(Sigma);
    M.Sigma = Sigma;
end
if ~isempty(p)
    M.PComponents = p;
end

end