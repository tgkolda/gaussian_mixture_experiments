function [mu, Sigma, p, fun] = vec2params(M, v, sig)
%VEC2PARAMS 

sz_mu = numel(M.mu);
size_mu = size(M.mu);

size_Sigma = size(M.Sigma);
sz_Sigma = prod(size_Sigma);

sz_p = numel(M.PComponents);
size_p = size(M.PComponents);

if nargin < 3 || isempty(sig)
    switch size(v(:),1)
        case sz_mu
            sig = 1;
        case sz_mu + sz_Sigma
            sig = 2;
        case sz_mu + sz_Sigma + sz_p
            sig = 3;
        otherwise
            error('v is not the right size')
    end
end

if nargout == 4
    fun = @vec2params_inner;
    
    [mu, Sigma, p] = fun(v);
else
    [mu, Sigma, p] = vec2params_inner(v);
end

    function [mu, Sigma, p] = vec2params_inner(v)

        mu = reshape(v(1:sz_mu), size_mu);
        
        if sig >= 2
            ptr = sz_mu;
            Sigma = reshape(v(ptr+(1:sz_Sigma)), size_Sigma);
        else
            Sigma = [];
        end
        
        if sig == 3
            ptr = ptr + sz_Sigma;
            p = reshape(v(ptr+(1:sz_p)), size_p);
        else
            p = [];
        end
    end

end
