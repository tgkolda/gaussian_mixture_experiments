function rel_err = RBF_error(Mtrue, Mest, eps)

if nargin < 3
    eps = 0;
end

[Mtrue_norm, ts] = dist_dot(Mtrue, Mtrue, eps);
[Mest_norm, es] = dist_dot(Mest, Mest, eps);
Mest_norm = Mest_norm * exp(es - ts);
[M_dot, ds] = dist_dot(Mtrue, Mest, eps);
M_dot = M_dot * exp(ds - ts);


err = Mtrue_norm + Mest_norm - 2 * M_dot;

rel_err = err / Mtrue_norm;

end

function [ddot, ms] = dist_dot(M1, M2, eps)
    
    if M1.SharedCovariance ~= M2.SharedCovariance || ...
           M1.CovarianceType ~= string(M2.CovarianceType)
        error("Not implemented yet")        
    end
    
    mu1T = M1.mu';
    mu2T = M2.mu';
    [n, r1] = size(mu1T);
    [~, r2] = size(mu2T);
    Sigma1 = reshape(M1.Sigma, n, r1);
    Sigma2 = reshape(M2.Sigma, n, r2);

    Sigma12 = Sigma1 + reshape(Sigma2, n, 1, r2) + eps;

    log_scale = -sum(log(Sigma12)) / 2;
    ms = max(log_scale(:));
    scale = exp(log_scale - ms);
    Q = sum((mu1T - reshape(mu2T, n, 1, r2)).^2 ./ Sigma12);
    ddot = M1.PComponents * shiftdim(exp(-Q/2).*scale) * M2.PComponents';

end

