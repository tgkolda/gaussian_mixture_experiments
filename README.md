# Gaussian Mixture Experiments

These are the experiments for the paper TBD:INSERT-PAPER-NAME-HERE.

# Requirements

- MATLAB w/ Statistics Toolbox
- Tensor Toolbox for MATLAB, version ???

# Experiments

Four categories of experiments:
1. **DEBIAS** Experiments using the debiased tensor. Mainly to show the improvement versus `sym_cp` to handle noise appropriately. These will be the least important, but it's a comparison to some of the only other work to consider larger-scale problems (i.e., large n) with moment methods. This may also include comparison to the EM  for fixed covariance `em_fixed_sigma`, but that might dilute the story. Still trying to decide.
  - One idea per Joao is to show that the isym objective function stagnates whereas isymd does not. 
  - Maybe also show how isymd can handle unbalanced weights and/or norms thanks to augmentation trick (though this trick is amenable to isym too).

2. **VEM** Comparison versus MATLAB's EM to show that the moment methods work where EM does not. This needn't be over a full range of possibilities. We just need to show it's the case for a few interesting examples. EM will always be faster, but the moment methods should be more robust.

3. Proof of "correctness" in all the scenarios you suggest: spherical, diagonal/isotropic, and full, with and without being shared. Showing that it's not so expensive.

4. **STOCH** Stochastic version, which should really helps with speed and be almost trivial to implement.

## Debiasing Experiments

The goal of these experiments is primarily to compare to `cp_isym` from the prior work of Sherman and Kolda (2020). 
