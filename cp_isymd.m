function [M,info,M0,varargin] = cp_isymd(X, r, Sigma, varargin)
%CP_ISYMD Implicit symmetric CP for debiased tensors moments of GMMs
%
%   M = CP_ISYMD(X, R, SIGMA) requires X to be a symktensor and R is the number
%   of desired components in the resulting symmetric CP stored in the
%   output symktensor M. SIGMA is the covariance of the noise. 
%   Typically, R << ncomponents(X). This function fits a symktensor M to
%   the debiased tensor by solving the optimization problem (34) in [1].
%
%   [M,INFO] = CP_ISYMD_OPT(X,R) returns additional information in INFO.
%
%   [...] = CP_ISYM_OPT(X,R,'param','value') takes additional arguments:
%
%      'method' - Optimization algorithm. Default:'lbfgsb'. 
%         o 'lbfgsb' (Quasi-Newton method with bound constraints),
%         o 'lbfgs' (Quasi-Newton method from Poblano Toolbox), 
%         o 'fminunc'(Quasi-Netwon from Optimization Toolbox),
%         o 'adam' (stochastic gradient descent with momentum).              
%         For optimzation algorithm choices and parameters, see
%         <a href="matlab:web(strcat('file://',fullfile(getfield(what('tensor_toolbox'),'path'),'doc','html','opt_options_doc.html')))">Documentation for Tensor Toolbox Optimization Methods</a>
%
%      'init' - Initialization for factor matrix, Default: 'rrf'. 
%         The user can pass in an explicit initial guess as a symktenor or
%         a factor matrix, or pass in a function handle to generate the matrix
%         given the size as two arugments. Alternatively, the user can specify 
%         the following string options:
%              - 'rrf' for the randomized range finder with Gaussian weights.
%              - 'rrf_simplex' for the rrf with weights sampled from the simplex
%       
%      'preprocess' - If set to true, centers and rescales the samples.
%         It is set to false by default.
%
%      'augconst' - Augmentation constant, denoted by ω in [1]. See 
%         Section 5.2.
%      
%      'augmode' - Augmentation mode picked. Options are 'no' (no
%         augmentation), 'implicit' and 'post-process' (default). 
%         See section 6.3 in [1] for more details.
%
%      'state' - Random state, to re-create the same outcome.
%
% [1] João M. Pereira, Joe Kileel, and Tamara G. Kolda. "Tensor Moments of 
%     Gaussian Mixture Models: Theory and Applications", arXiv:2202.06930 (2022). 
%     <a href="https://arxiv.org/abs/2202.06930">https://arxiv.org/abs/2202.06930</a>
%
%   <a href="matlab:web(strcat('file://',fullfile(getfield(what('tensor_toolbox'),'path'),'doc','html','cp_isym_doc.html')))">Additional documentation for CP-ISYM</a>
%
%   See also SYMKTENSOR/CP_ISYM.
%
% Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>
%
% Code by Joao M. Pereira and Tamara Kolda, based on the code of cp_isym

%%
tic; % Start timer for setup costs - finished inside optimization call!

%% Error Checking
if ~isa(X,'symktensor')
    error('Ximp must be a symktensor');
end

%% Random set-up
defaultStream = RandStream.getGlobalStream;

%% Algorithm Parameters
params = inputParser;
params.KeepUnmatched = true;
params.PartialMatching = false;
params.addParameter('state', defaultStream.State);
params.addParameter('init','rrf');
params.addParameter('method','lbfgsb');
params.addParameter('preprocess',false);
params.addParameter('augmode','postprocess');
params.addParameter('augconst',0.1);
params.addParameter('fsamp', 1000);
params.addParameter('gsamp', 100);
params.addParameter('printitn',1);

params.parse(varargin{:});

%% Initialize random number generator with specified state
defaultStream.State = params.Results.state;

%% Setup
init = params.Results.init;
method = params.Results.method;
preprocess = params.Results.preprocess;
augmode = params.Results.augmode;
c = params.Results.augconst;
fsamp = params.Results.fsamp;
gsamp = params.Results.gsamp;
printitn = params.Results.printitn;

optopts = params.Unmatched;
optopts.printitn = printitn;

% Save
info.params = params.Results;

%% Extract sizes, etc.
nd = ndims(X);
n = size(X,1);

%% Preprocess data
if preprocess
    n = size(X.u, 1);
    meanX = zeros(n, 1);%meanX = mean(X.u, 2);%
    u = X.u - meanX;
    scaleX = sqrt(mean(X.u.^2, 2) * n);
    u = u ./ scaleX;
    Sigma = Sigma ./ (scaleX .* scaleX');
    X = symktensor(X.lambda, u, nd);
end

%% Initial guess
if isa(init,'symktensor')
    M0 = init;
    info.params.init = '[user-provided symktensor]';
else
    N = size(X.u, 2);
    if isa(init,'function_handle')
        A0 = init(n,r);
    elseif strcmpi(init,'rrf')
        A0 = matrandnorm( X.u * randn(N, r) );
    elseif strcmpi(init,'rrf_simplex')
        sp = -log(rand(N, r));
        sp = sp ./ sum(sp);
        A0 = X.u * sp;
    elseif isequal(size(init),[n,r])
        if preprocess
            init = (init - meanX) ./ scaleX;
        end
        A0 = init;
        info.params.init = '[user-provided matrix]';
    else
        error('Invalid ''init'' option');
    end
    lambda0 = ones(r,1) / r;
    M0 = symktensor(lambda0,A0,nd);
end
if ncomponents(M0) ~= r
    error('Initial guess has %d components but expected %d components', ncomponents(M0), r);
end

switch augmode
case {"no", "none","no augmentation"}
    warning("The no augmentation mode may give solutions " + ...
            "that are not scaled correctly");
    fghX = @(x, X) fg_implicit_debias(update(M0, x), X, Sigma, true);
    M0_vec = M0.u(:);

case "postprocess"

    % Data
    X = symktensor(X.lambda, [X.u; c*ones(1,ncomponents(X))], nd);

    % Initial Model
    M0 = symktensor(M0.lambda, [M0.u; c*ones(1,r)], nd);
    
    % Sigma
    Sigma = [Sigma, zeros(n,1); zeros(1,n+1)];

    fghX = @(x, X) fg_implicit_debias(update(M0, x), X, Sigma, true);
    M0_vec = M0.u(:);

case "implicit"
    fghX = @(x, X) fg_implicit_debias(update(M0, x), X, Sigma, false, c);
    M0_vec = tovec(M0);

otherwise
    error('Augmentation mode not implemented')
end

%% Shared options
optopts.xdesc = sprintf('Order, Size: %d, %d', ndims(X), size(X,1));

%% Finish setup
setuptime = toc; 

%% Optimization
tic
if (printitn > 0)
    fprintf('\nCP-ISYM Implicit Symmetric CP Optimization');
end
switch(method)
    case {'lbfgsb','lbfgs','fminunc'}
        fgh = @(mvec) fghX(mvec, X);
        optname = sprintf('tt_opt_%s', method);
        [x,f,optinfo] = feval(optname, M0_vec, fgh, optopts);

    case {'adam'}
        if strcmpi(fsamp,'exact')
            optopts.fexact = true;
            XF = X;
            optopts.fdesc = 'Function: exact';
        else
            optopts.fexact = false;
            XF = rand_extract(X, fsamp);
            if isscalar(fsamp)
                optopts.fdesc = sprintf('Function: %d samples out of %d observations', fsamp, ncomponents(X));
            else
                optopts.fdesc = sprintf('Function: %d user-specified samples out of %d observations', length(fsamp), ncomponents(X));
            end
        end
        fh = @(mvec) fghX(mvec, XF);
        gh = only_grad(@(mvec) fghX(mvec, rand_extract(X, gsamp)));
        optopts.gdesc = sprintf('Gradient: using %d samples  out of %d observations', gsamp, ncomponents(X));       
        [x,f,optinfo] = tt_opt_adam(M0_vec, fh, gh, optopts);
        
    otherwise
        error('Invalid method')
end
opttime = toc;

M = update(M0, x);

%% Clean up
if ismember(augmode, ["no", "none","no augmentation"])
    M = normalize(M);
elseif augmode == "postprocess"
    M = aug_normalize(M, c);
end

if preprocess
    u = M.u .* scaleX + meanX;
    M = symktensor(M.lambda, u, nd);
end

%% Save results

info.f = f;
info.optout = optinfo;
info.opttime = opttime;
info.setuptime = setuptime;
info.opttime = opttime;
info.setuptime = setuptime;

end


function X = aug_normalize(X, scale)
%AUG_NORMALIZE Normalizes the columns of the augmented GMM optimization
%solution

A = X.u;

scale = A(end,:)/scale;
A = A(1:end-1,:)./scale;

nd = ndims(X);
lambda = X.lambda .* scale'.^nd;

X = symktensor(lambda, A, nd);

end
