function metrics = debias_eval_soln(lambda_est, A_est, X, lambda_true, A_true, sigma, runtime, randstate)

%%

[n,m] = size(A_est);
Sigma = sigma^2 * ones(1,1,m);
M_est = gmmodel(A_est', Sigma, lambda_est');
M_true = gmmodel(A_true',Sigma, lambda_true');

%% LogPDF

metrics.logpdf = sum(log_pdf(M_est,X'));


%% MoM Objective Function
r = size(X,2);
tM = symktensor(lambda_est,A_est,3);
tX = symktensor(1/r*ones(r,1),X,3);
metrics.dmom3func = fg_implicit_debias(tM,tX,sigma^2*eye(n));
tM = symktensor(lambda_est,A_est,4);
tX = symktensor(1/r*ones(r,1),X,4);
metrics.dmom4func = fg_implicit_debias(tM,tX,sigma^2*eye(n));

%% Save runtime
if exist('runtime','var')
    metrics.runtime = runtime;
end

%% Save randstate
if exist('randstate','var')
    metrics.randstate = randstate;
end

%% Compute the matching

if ~exist('M_true','var')
   return
end

mu_true = M_true.mu';
m_true = M_true.NumComponents;
mu = M_est.mu';
m_est = M_est.NumComponents;
D2 = repmat(vecnorm(mu_true).^2',1,m_est) + repmat(vecnorm(mu).^2,m_true,1) - 2*(mu_true'*mu);
D = sqrt(max(0,D2));
matching = matchpairs(D,1000);
perm_true = matching(:,1);
perm_est = matching(:,2);
metrics.perm_true = perm_true;
metrics.perm_est = perm_est;

%% Relative distance in weights
w_true = M_true.ComponentProportion(perm_true);
w_est = M_est.ComponentProportion(perm_est);
weight_err = norm(w_true-w_est, 1);
metrics.weight_err = weight_err;

%% Relative distance in means
mu_true = M_true.mu(perm_true,:)';
mu_est = M_est.mu(perm_est,:)';
mu_nrmerr = vecnorm(mu_true-mu_est);
mu_relerr = mu_nrmerr./vecnorm(mu_true);
mu_relerr_avg = mean(mu_relerr);
metrics.mu_nrmerr = mu_nrmerr;
metrics.mu_relerr = mu_nrmerr;
metrics.mu_relerr_avg = mu_relerr_avg;

%% Cosines
metrics.mu_cosines = dot(mu_true./vecnorm(mu_true), mu_est./vecnorm(mu));
metrics.avg_mu_cosine = mean(metrics.mu_cosines);

%% Sigma Diags
sigmadiag_true = reshape(M_true.Sigma(:,:,perm_true),[],m_true);
sigmadiag_est = reshape(M_est.Sigma(:,:,perm_est),[],m_est);
metrics.sigma_relerr = vecnorm(sigmadiag_true-sigmadiag_est)./vecnorm(sigmadiag_true);
metrics.sigma_relerr_avg = mean(metrics.sigma_relerr);

