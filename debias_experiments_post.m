%% Post-process results of diagcov_experiments_run.m

%% Load results
%load debias.dat

%% Setup some distinct colors
colors = [228,26,28
    55,126,184
    77,175,74
    152,78,163
    255,127,0]/255;

%% Boxplots - Logpdf

fieldname = 'logpdf';

figure(1); clf
%
mm = 2; nn = 3;

list = {'logpdf','dmom3func','runtime','weight_err',...
    'mu_relerr_avg','avg_mu_cosine'};
titles = {'EM Obj.','MM3 Obj.',...
    'Runtime','\lambda error',...
    '\mu error','\mu cosine'};
ylabels = {'logpdf','||X3-M3||^2-||X3||^2','time (sec)','one-norm err',...
    'avg two-norm relerr','avg cosine'};
for i = 1:length(list)
    subplot(mm,nn,i);
    dataplot(results,list{i},titles{i},ylabels{i});
    set(gca,'XTickLabelRotation',45)
    xlim([0 4])

end

%Fixes

subplot(mm,nn,5)
yl = ylim;
ylim([0 yl(2)])


subplot(mm,nn,6)
yl = ylim;
ylim([min(yl(1),0.9),1])

% data = cellfun(@(x) x.(fieldname), results)
% 
% figure(1); clf;
% %boxplot(data)
% for i = 1:3
%     low = min(data(:,i));
%     mid = mean(data(:,i));
%     hi = max(data(:,i));
%     plot([i i], [low hi], '-', 'Color',colors(i,:),'LineWidth',3);
%     hold on;
% end
% xlim([0.5 3.5])

%%


function dataplot(results,fieldname,titlestr,ylabelstr)

data = cellfun(@(x) x.(fieldname), results);
labels=[1:size(data,2)];
metalabels = repmat(labels,size(data,1),1);

if 0
    h=boxchart(metalabels(:),data(:),'GroupByColor',metalabels(:),'BoxWidth',1);
else
    C = [228,26,28; 55,126,184; 77,175,74; 152,78,163; 255,127,0]/255;
    scatter(metalabels(:),data(:),50,C(metalabels(:),:),...
        'MarkerFaceColor','flat','MarkerFaceAlpha',0.25)
    
end
    title(titlestr)
    set(gca,'XTick',1:size(data,2))
    set(gca,'XTickLabel',{'EM','TD','MM'})
    ylabel(ylabelstr)
end