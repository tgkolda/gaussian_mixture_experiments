%% Script to run debias experiments

clear
artifact_prefix = 'debias_experiments';
timestamp = datestr(now,'yyyy-mm-dd-HHMM');
diaryname = sprintf('%s_%s.log',artifact_prefix,timestamp);
datafilename = sprintf('%s_%s.mat',artifact_prefix,timestamp);
diary off
diary(diaryname)
fprintf('------------------------------------------------------------\n');
fprintf('Experimental Log: %s\n',diaryname);
fprintf('------------------------------------------------------------\n');
fprintf('\n');


%% Setup
fprintf('--- Setup ---\n')
echo on
n = 500; % Fixed size
cong = 0.5; % Fixed congruence
m = 10;
sigma = 0.3;
dvals = 3;

nsamplesper = 300; % number of samples per cluster, on average
echo off
fprintf('-------------\n\n');


%% Create problem instance
fprintf('--- Creating Problem Instance ---\n')
echo on

randstate = randi(1e9); fprintf('randstate=%d\n',randstate);
rng(randstate);

% Create model parameters and model
[mu_true,w_true,gmd,info] = debias_experiments_setup(n,m,sigma^2,'cong',cong);

% Realizations
r = m*nsamplesper; % number of realizations/observations
X = transpose(random(gmd,r));

% Setup evaluator
solneval = @(w,mu,r) debias_eval_soln(w,mu,X,w_true,mu_true,r);

fprintf('Dimension: %d\n',n);
fprintf('Number of components: %d\n',m);
fprintf('sigma: %g\n',sigma);
fprintf('Min Weight: %g\n', info.params.minwgt);
fprintf('Congruence of (Normalized) Means: %g\n', info.params.cong);
fprintf('Length Mean: %g\n', info.params.lmean);
fprintf('Length Std: %g\n', info.params.lstd);
fprintf('Number of realizations: %d\n', r);
fprintf('---------------------------------\n\n')

%% Export data
fprintf('--- Exporting Problem Instance---\n')
echo on
writematrix(mu_true','debias_mu.csv','Delimiter',',');
writematrix(w_true,'debias_lambda.csv','Delimiter',',');
writematrix(X','debias_observations.csv','Delimiter',',');
echo off
fprintf('---------------------------------\n\n')


%% MAIN LOOP

fprintf('--- Main Loop ---\n')
maxmethods = 5;
nruns = 10;
results = cell(nruns, maxmethods);
method = cell(maxmethods,1);

opts = struct();
opts.printitn = 0;
opts.ftol=1e-14;


for idxrun = 1:nruns
    
    idxmethod = 0;
    
    %% Create initial guesses
    fprintf('Setting up RRF Initial Guesses for Trial %d with m=%d\n',idxrun,m)
    
    randstate = randi(1e9);
    fprintf('\trandstate=%d\n',randstate);
    rng(randstate);
    
    R = rand(r,m);
    R = R ./ sum(R);
    X0 = X*R;
    
    %% Method: EM
    idxmethod = idxmethod+1;
    if (idxrun == 1)
        method{idxmethod} = 'EM';
    end
    fprintf('Running EM (Method %d), Trial %d...\n',idxmethod,idxrun);
    tic
    [w,mu] = em_fixed_sigma(X,sigma^2*eye(n),m,'printitn',0,'Mu0',X0);
    runtime = toc;    
    results{idxrun, idxmethod} = collect_results(solneval,runtime, w', mu);
    
    %% cp_isym with d=3,4
    
    for d = dvals
        idxmethod = idxmethod+1;
        if (idxrun == 1)
            method{idxmethod} = sprintf('cp_isym with d=%d',d);
        end
        XX = symktensor(1/r*ones(r,1),X,d);
        fprintf('Running %s (Method %d), Trial %d...\n',method{idxmethod},idxmethod,idxrun);
        tic
        [K,info] = cp_isym(XX,m,'init',X0,opts);
        runtime = toc;
        foo = collect_results(solneval, runtime, K.lambda, K.u);
        results{idxrun, idxmethod} = foo;
    end
    
    %% cp_isymd
    c = 1;
    for d = dvals
        idxmethod = idxmethod+1;
        if (idxrun == 1)
            method{idxmethod} = sprintf('cp_isymd with d=%d',d);
        end
        XX = symktensor(1/r*ones(r,1),X,d);
        fprintf('Running %s (Method %d), Trial %d...\n',method{idxmethod},idxmethod,idxrun);
        Sigma = sigma^2*eye(n);
        tic
        [K,info] = cp_isymd(XX,m,Sigma,opts,'init',X0,'augconst',c);
        runtime = toc;
        foo = collect_results(solneval, runtime, K.lambda, K.u);
        results{idxrun, idxmethod} = foo;
    end
    fprintf('-----------------\n\n')

    
    
end

%% Clean up
results = results(:,1:idxmethod);
method = method(1:idxmethod);
%% Clean up
fprintf('--- Saving Results ---\n');
fprintf('----------------------\n\n');
fprintf('\tFilename: %s\n', datafilename);
save(datafilename);
diary off
fprintf('----------------------\n\n');

debias_experiments_post
%%
function foo = collect_results(solneval, runtime, w, mu, varargin)

foo = solneval(w,mu,runtime);
foo.runtime = runtime;
foo.extra = varargin;
fprintf('\tRuntime: %f\n', foo.runtime);
fprintf('\tCosine : %f\n', foo.avg_mu_cosine);
fprintf('\tloglik : %g\n', foo.logpdf);
fprintf('\tfunc3  : %g\n', foo.dmom3func);

end