function [muvecs,lambda,gmd,info] = debias_experiments_setup(n,ncomp,sigmasqr,varargin)
%SETUP_DEBIAS_EXPERIMENT Setup Gaussian Mixture for debiasing experiments.
%

%% Random set-up
defaultStream = RandStream.getGlobalStream;

%% Set algorithm parameters from input or by using defaults
params = inputParser;
params.addParameter('minwgt',1/ncomp);
params.addParameter('cong',0.5);
params.addParameter('state', defaultStream.State);
params.addParameter('lmean',1);
params.addParameter('lstd',0);
params.parse(varargin{:});

% Save info
info.params = params.Results;

%% Initialize random number generator with specified state
defaultStream.State = params.Results.state;

%% Create means
muvecs = matrandcong(n,ncomp,params.Results.cong);

%% Adjust lengths
newl = params.Results.lmean + params.Results.lstd*randn(1,ncomp);
muvecs = muvecs .* newl;


% Save info
info.newl = newl;

%% Create sigma
sigmadiag = sigmasqr * ones(1,n);

%% Create weights
rho = ncomp * params.Results.minwgt;
x = -log(rand(ncomp,1));
y = ones(ncomp,1);
lambda = (1-rho)*x/sum(x) + rho*y/sum(y);

%% Create Gaussian Mixture Distribution
gmd = gmdistribution(muvecs',sigmadiag,lambda');