%% Post-process results of diagcov_experiments_run.m

%% Load results
%load diagcov.dat

%% Setup some distinct colors
colors = [228,26,28
    55,126,184
    77,175,74
    152,78,163
    255,127,0]/255;

%% Boxplots - Logpdf

fieldname = 'logpdf';

figure(1); clf
%
mm = 2; nn = 4;

list = {'logpdf','mom3func','mom4func','runtime','weight_err',...
    'mu_relerr_avg','sigma_relerr_avg','avg_mu_cosine'};
titles = {'EM Obj.','MM3 Obj.','MM4 Obj.',...
    'Runtime','\lambda error',...
    '\mu error','diag(\Sigma) error','\mu cosine'};
ylabels = {'logpdf','||X3-M3||^2-||X3||^2','||X4-M4||^2-||X4||^2','time (sec)','one-norm err',...
    'avg two-norm relerr','avg two-norm relerr','avg cosine'};
for i = 1:length(list)
    subplot(mm,nn,i);
    dataplot(results,list{i},titles{i},ylabels{i});
    set(gca,'XTickLabelRotation',45)
    xlim([0 4])

end

%Fixes
for i = 5:7
subplot(mm,nn,i)
yl = ylim;
ylim([0,yl(2)])
end
% data = cellfun(@(x) x.(fieldname), results)
% 
% figure(1); clf;
% %boxplot(data)
% for i = 1:3
%     low = min(data(:,i));
%     mid = mean(data(:,i));
%     hi = max(data(:,i));
%     plot([i i], [low hi], '-', 'Color',colors(i,:),'LineWidth',3);
%     hold on;
% end
% xlim([0.5 3.5])

%%


function dataplot(results,fieldname,titlestr,ylabelstr)

data = cellfun(@(x) x.(fieldname), results);
labels=[1 2 3];
metalabels = repmat(labels,10,1);

if 0
    h=boxchart(metalabels(:),data(:),'GroupByColor',metalabels(:),'BoxWidth',1);
else
    C = [228,26,28; 55,126,184; 77,175,74; 152,78,163; 255,127,0]/255;
    scatter(metalabels(:),data(:),50,C(metalabels(:),:),...
        'MarkerFaceColor','flat','MarkerFaceAlpha',0.25)
    
end
    title(titlestr)
    set(gca,'XTick',[1 2 3])
    set(gca,'XTickLabel',{'EM','MM3','MM4'})
    ylabel(ylabelstr)
end