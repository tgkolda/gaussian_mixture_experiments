%% DIAG COVARIANCE EXPERIMENTS
% Versus EM, for gmm_mopt, for arbitrary diagonal covariance matrices
clear
artifact_prefix = 'diagcov_experiments';
timestamp = datestr(now,'yyyy-mm-dd-HHMM');
diaryname = sprintf('%s_%s.log',artifact_prefix,timestamp);
datafilename = sprintf('%s_%s.mat',artifact_prefix,timestamp);
diary off
diary(diaryname)
fprintf('------------------------------------------------------------\n');
fprintf('Experimental Log: %s\n',diaryname);
fprintf('------------------------------------------------------------\n');
fprintf('\n');
%% Parameters
fprintf('--- Setup ---\n')
echo on
n = 100;
m = 20;
cong = 0.5;
d = 3;
avgsigma = 0.2;
minlambda = 1/(5*m);
numper = 400;
r = m*numper;
echo off
fprintf('-------------\n\n');

%% Create GMM
fprintf('--- Creating Problem Instance ---\n')
echo on
% Fixing random state for reproducibility
randstate = 910171400;
%randstate = randi(1e9); fprintf('randstate=%d\n',randstate); 
rng(randstate);
% Correlated mean vectors
A = matrandcong(n, m, cong);
Mu = A';
% Diagonal covariances
SigmaDiags = avgsigma^2*(0.5 + rand(n,m));
Sigma = reshape(SigmaDiags,[1 n m]);
% Proportions
x = -log(rand(m,1));
Lambda = (1 - (m*minlambda))*x/sum(x) + minlambda*ones(m,1);
clear x
% Create Gaussian Mixture Model
M = gmmodel(Mu, Sigma, Lambda);
% Generate Random Observations
X = M.random(r);
echo off
results_true = diagcov_eval_soln(M,X);
disp_results('True',results_true)
fprintf('---------------------------------\n\n')

%% Export data
fprintf('--- Exporting Problem Instance---\n')
echo on
writematrix(Mu,'diagcov_mu.csv','Delimiter',',');
writematrix(SigmaDiags,'diagcov_sigmadiags.csv','Delimiter',',');
writematrix(Lambda,'diagcov_lambda.csv','Delimiter',',');
writematrix(X,'diagcov_observations.csv','Delimiter',',');
echo off
fprintf('---------------------------------\n\n')

%%
fprintf('--- Main Loop ---\n')
nruns = 10;
results = cell(nruns,3);
for runidx = 1:nruns
    
    % -- EM --
    fprintf('Running EM for Run %d...\n', runidx);
    echo on
    randstate = randi(1e9); fprintf('randstate=%d\n',randstate); rng(randstate);
    tic
    Mem = fitgmdist(X,m,'CovarianceType','diagonal','Options',statset('Display','off','MaxIter',500),'Start','randSample');
    runtime_em = toc;
    echo off
    results{runidx,1} = diagcov_eval_soln(Mem,X,[],M,runtime_em,randstate);
    disp_results('EM',results{runidx,1})
    
    %% -- MoM-3 ---
    fprintf('Running MoM-3 for Run %d...\n', runidx)
    echo on
    fprintf('randstate=%d\n',randstate); rng(randstate);
    tic
    Mmom3 = gmm_mopt(X,m,3,'printitn',0,'augconst',.5);
    runtime_mom3 = toc;
    echo off
    results{runidx,2} = diagcov_eval_soln(Mmom3,X,[],M,runtime_mom3,randstate);
    disp_results('MoM-3',results{runidx,2})
    
    %% --- MoM-4
    fprintf('Running MoM-4 for Run %d...\n', runidx)
    echo on
    fprintf('randstate=%d\n',randstate); rng(randstate);
    tic
    Mmom4 = gmm_mopt(X,m,4,'printitn',0,'augconst',.5);
    runtime_mom4 = toc;
    echo off
    results{runidx,3} = diagcov_eval_soln(Mmom4,X,[],M,runtime_mom4,randstate);
    disp_results('MoM-4',results{runidx,3})
end
fprintf('-----------------\n\n')

%% Clean up
fprintf('--- Saving Results ---\n');
fprintf('----------------------\n\n');
fprintf('\tFilename: %s\n', datafilename);
save(datafilename);
diary off
fprintf('----------------------\n\n');

%%

function disp_results(name,results)

fprintf('%s results...\n', name);

check_and_print(results, 'logpdf');
check_and_print(results, 'mom3func');
check_and_print(results, 'mom4func');
check_and_print(results, 'weight_relerr');
check_and_print(results, 'mu_relerr_avg');
check_and_print(results, 'avg_mu_cosine');
check_and_print(results, 'runtime');

end

function check_and_print(results,fname)
if isfield(results,fname)
    fprintf('\t%s: %g\n', fname, results.(fname));
end
end