function [w,Mu] = em_fixed_sigma(X, Sigma, m, varargin)
%GMM_EM_SIGMA Estimate Gaussian mixture model with known Sigma
%
%  [W,MU] = GMM_EM_SIGMA(X,SIGMA,R) computes the means of the Gaussian
%  mixture model with known covariance. Here X is an N x P matrix of P
%  observations, SIGMA is the N x N covariance matrix, and R is the number
%  of components. It returns W, the M weights of the GMM, and MU, an N x M
%  matrix of the component means. 
%
%  **NOTE**: The vector and matrix orientiations are different than those
%  expected by MATLAB's GMM functionality.

%% Random set-up
defaultStream = RandStream.getGlobalStream;

%% Algorithm Parameters
params = inputParser;
params.addParameter('state', defaultStream.State);

params.addParameter('printitn',1);
params.addParameter('w0',[]);
params.addParameter('Mu0',[]);
params.addParameter('maxiters',1000);
params.addParameter('tol',1e-4);

params.parse(varargin{:});

%% Initialize random number generator with specified state
defaultStream.State = params.Results.state;

%% Extract relevant parameters
printitn = params.Results.printitn;
maxiters = params.Results.maxiters;
tol = params.Results.tol;

%% Preprocessing

[~, p] = size(X);
[V, D] = eig(Sigma); % Sigma = V*D*V'
DINV2 = sqrt(1 ./ diag(D));
VX = (DINV2 .* (V * X))';
nVX = vecnorm(VX, 2, 2).^2;

%% Initialize the method
w = params.Results.w0;
if isempty(w)
    w = 1/m * ones(1,m);
end

Mu = params.Results.Mu0;
if isempty(Mu)
    Mu = X * sqrt(1/p) * randn(p,m); % Randomized range finder initialization
end

%% EM procedure
for iter = 1:maxiters
    
    % Save old MU
    MuOld = Mu;
    
    % Estimate set memberships
    % P_log(i,j) = log(Probability of observation i in component j)
    % Use formula \|x - y\|^2 = \|x\|^2 + \|y\|^2 - 2 x.y
    VMU = DINV2.*(V*Mu);
    nVMU = vecnorm(VMU).^2;
    P_log = -0.5 * (nVX + nVMU - 2 * VX*VMU);
    
    % Since H = P / sum(P,2), we can divide
    % the columns of P so that in each row
    % one of the values is at least one
    % this improves EM stability
    P_log = P_log - max(P_log,[],2);
    
    % Similar changes to
    % improve EM stability
    P_log_m = max(P_log);
    P_log_s = P_log - P_log_m;
    H_s = exp(P_log_s);
    P_m = exp(P_log_m);
    H_s = H_s ./ (H_s * P_m');
    
    % Recalculate w and MU
    w = mean(H_s).*P_m;
    Mu = X*H_s ./ sum(H_s);
    
    % Check for convergence
    reldiff = norm(Mu(:)-MuOld(:),'fro')/norm(Mu(:),'fro'); 
    if (mod(iter,printitn)==0)
        fprintf('Iteration = %3d, Rel. Diff. in Mu = %g\n', iter, reldiff);
    end
    if reldiff < tol
        break;
    end
    
end