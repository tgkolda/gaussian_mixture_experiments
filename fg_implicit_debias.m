function [f, g] = fg_implicit_debias(tM, tA, Sigma, fixed_lambda, aug)
%FG_IMPLICIT_DEBIAS Function and gradient of F(M)=||A-M||^2 where M is a 
% symktensor and A is the tensor moment of a GMM with covariance Sigma, as
% in [1], Equation (34).
%
%   [F,G] = FG_IMPLICIT_DEBIAS(M, A, SIGMA) requires *both* M
%   and A to be symktensor objects and computes the function (F) and
%   corresponding gradient (G) of
%
%      F(M) = ||Y-M||^2 -||Y||^2 = - 2 <M,Y> + ||M||^2
%
%   where Y is the debiased moment tensor of the samples in X coming from a
%   GMM with covarianve SIGMA. Because the ||Y||^2 term does not depend on
%   M, it can be ignored in the context of optimization. The gradient G is
%   returned in vectorized form. If M is a d-way, n-dimensional, rank-r
%   symktensor, then G is a vector of length n*r.
%
%   OPTIONAL PARAMETERS:
%      - aug:   Implicitly consider the augmented system with constant
%               equal to aug. Set to 0 to turn off the augmented system.
%               The default value is 0.
%      - fixed_lambda: If set to true, provides the gradient of F as if.
%               lambda is constant. The default value is false.
%
%
%   See also CP_ISYMD, SYMKTENSOR/FG_IMPLICIT
%
% [1] João M. Pereira, Joe Kileel, and Tamara G. Kolda. "Tensor Moments of 
%     Gaussian Mixture Models: Theory and Applications", arXiv:2202.06930 (2022). 
%     <a href="https://arxiv.org/abs/2202.06930">https://arxiv.org/abs/2202.06930</a>
%
% Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>
%
% Code by Joao M. Pereira and Tamara Kolda, based on the code of fg_implicit

%% extract data

if nargin<4; fixed_lambda = false; end
if nargin<5; aug = 0; end

aug2 = aug^2;

d = ndims(tA);
A = tA.u;
eta = reshape(tA.lambda, 1, []);

Mu = tM.u;
lambda = reshape(tM.lambda, 1, []);

%% do computations

SigmaMu = Sigma * Mu;
MuSigmaMu = dot(Mu, SigmaMu);

AMu = A'*Mu + aug2;

alpha_prev = 1;
alpha_curr = AMu;

% Calculate alpha's
% Only save last two
for m=2:d-1
    aux = alpha_curr;
    alpha_curr = alpha_curr .* AMu - ((m - 1) * MuSigmaMu) .* alpha_prev;
    alpha_prev = aux;
end

% This calculates gradient of mu
% Missing here 2 * d * lambda (added later)
Y = A * (alpha_curr.* eta') - ((d-1) * (eta * alpha_prev)) .* SigmaMu;

% This formula sets w to alpha(d) summed over all samples
w = dot(Mu,Y);
if aug2>0
    w = w + aug2 * (eta * alpha_curr);
end

% This part of the derivative is the same as cp_isym
G = Mu'*Mu + aug2;
Gd1 = G.^(d-1);
u = lambda*(Gd1.*G);

f = dot(lambda,u) - 2 * dot(lambda,w);

g_lambda = 2 * (u - w);

% Changed '* diag(lambda)' to '.* lambda'
g_Mu = 2 * d * ((Mu .* lambda) * Gd1 - Y) .* lambda;

if fixed_lambda
    g = g_Mu(:);
else
    g = [g_lambda(:); g_Mu(:)];
end
