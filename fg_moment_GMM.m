function [varargout] = fg_moment_GMM(M, X, d, varargin)
%FG_MOMENT_GMM Function and gradient of F(M)=||X-M||^2 where M is a 
% the moments of a gmmodel and X is the empirical tensor moment, as in
% [1], Equation (30).
%
%   [F,G] = fg_moment_GMM(M, X, d) requires
%     - M to be a gmmodel;
%     - X to be a N x n matrix where the rows are samples;
%     - d to be a positive integer number;
%    and computes the function value (F) and corresponding gradient (G),
%    in terms of the parameters of M, of
%
%      F(M) = ||Y-T||^2 = ||Y||^2 - 2 <T, Y> + ||T||^2
%
%   where Y is the empirical d-th moment tensor of the samples in X  
%   coming from a GMM, T is the d-th moment of the GMM defined by M and 
%   the value of ||Y||^2 is provided in fX. The terms <T, Y> and ||T||^2
%   are calculated implicitly using the formulas in [1]. Because 
%   the term ||Y||^2 does not depend on M, it can be ignored (replaced
%   by zero) in the context of optimization. The gradient G is returned
%   in vectorized form.
%   
%   OPTIONAL PARAMETERS:
%      - fx:    Value of ||Y||^2. The default value is 0.
%      - aug:   Implicitly consider the augmented system with constant
%               equal to aug. Set to 0 to turn off the augmented system.
%               The default value is 0.
%      - fixed_lambda: If set to true, provides the gradient of F as if
%               lambda is constant. The default value is false.
%
%   See also GMM_MOPT.
%
% [1] João M. Pereira, Joe Kileel, and Tamara G. Kolda. "Tensor Moments of 
%     Gaussian Mixture Models: Theory and Applications", arXiv:2202.06930 (2022). 
%     <a href="https://arxiv.org/abs/2202.06930">https://arxiv.org/abs/2202.06930</a>
%
% 
%Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>
%
% Code by Joao M. Pereira and Tamara Kolda, based on the code of fg_implicit

%% extract data

params = inputParser;
params.addOptional('fX', 0);
params.addOptional('aug', 0);
params.addOptional('fixed_lambda', false);
params.addOptional('vfh', false);
params.addOptional('postprocess', false);
params.parse(varargin{:});

opts = params.Results;
fX = opts.fX;
aug = opts.aug;
fixed_lambda = opts.fixed_lambda;

SharedCovariance = M.SharedCovariance;
CovarianceType = M.CovarianceType;

Mp = M.PComponents;

r = M.NumComponents;

[N, n] = size(X);

vec_factorial = ones(d+1,1);

for k_ = 2:d
    vec_factorial(k_ + 1) = factorial(k_);
end

if CovarianceType == "full"
    zero_out_indices = tril(true(n),-1);
    if ~SharedCovariance
         zero_out_indices = zero_out_indices | false(1,1,r);
    end
    if opts.postprocess
        zero_out_indices(end, :, :) = true;
        zero_out_indices(:, end, :) = true;
    end
end

if opts.vfh
    sig = 3 - fixed_lambda;
    [~, ~, ~, M_vec2params] = M.vec2params(M.tovec(), sig);
    varargout{1} = @fg_moment_GMM_vecinput;
else
    [varargout{1:2}] = fg_moment_GMM_inner(X, M.mu, M.CholSigma, M.PComponents);
end



    function [f, g] = fg_moment_GMM_inner(X, mu, CholSigma, p)

    %% Dot-product computations
    if CovarianceType == "full"
        CholSigma(zero_out_indices) = 0;
    end
    
    if SharedCovariance
        nCov = 1;
    else
        nCov = r;
    end
    

    if d==1
        mean_X = mean(X);
        g_dot_p = -2*(mu*mean_X')';
        g_dot_sigma = zeros(size(CholSigma));
        g_dot_mu = -2*(p' * mean_X);
    else
        switch CovarianceType
          case "full"
            for k=nCov:-1:1
              XSigmaX(k, :) = vecnorm(X * CholSigma(:, :, k)', 2, 2).^2;
            end
          case "diagonal"
            X2 = X.^2;
            Sigma = CholSigma .^2;
            XSigmaX = (X2 * reshape(Sigma, n, nCov))';
          case {"isotropic", "spherical"}
            Sigma = CholSigma .^2;
            XSigmaX = reshape(Sigma, nCov, 1) * vecnorm(X,2,2)'.^2;
          otherwise
            error('Covariance Type not supported');          
        end
        
        muX = (X*mu')' + aug.^2;
        
        alpha_prev = ones(size(muX));
        alpha_curr = muX;
        
        for m=2:d-1
            aux = alpha_curr;
            alpha_curr = alpha_curr .* muX + ((m - 1) * XSigmaX) .* alpha_prev;
            alpha_prev = aux;
        end
        
        g_dot_mu = -(2 * d / N) * ((alpha_curr * X)  .* p');
        
        if SharedCovariance
            alpha_sigma = p*alpha_prev;
        else
            alpha_sigma = p'.*alpha_prev;
            g_dot_sigma = zeros(size(CholSigma));
        end
            
        switch CovarianceType
          case "full" 
            for k=1:size(alpha_sigma,1) 
                g_sigma_k = (X'.* alpha_sigma(k,:)) * X;
                g_dot_sigma(:,:,k) = (g_sigma_k + g_sigma_k') / 2;
            end
          case "diagonal"
                g_dot_sigma = reshape((alpha_sigma * X2)',1,n,[]);
          case {"isotropic", "spherical"}
                g_dot_sigma = reshape((vecnorm(X, 2, 2)'.^2)* alpha_sigma',1,1,[]);
        end
        
        g_dot_sigma = (-d * (d-1) / N) * g_dot_sigma;
            
        g_dot_p = - 2 * mean(alpha_curr .* muX + ((d - 1) * XSigmaX) .* alpha_prev, 2);
        g_dot_p = g_dot_p.'; 
    end

    f_dot = dot(p, g_dot_p);
    
    %% Norm computations
    
    muT = mu';    
    pp = 2 * (p'*p);

    if CovarianceType == "full"
        Sigma = pagemtimes(CholSigma, 'ctranspose',...
                           CholSigma, 'none' );

        g_norm_muT = zeros(n, r);
        g_norm_sigma = zeros(n, n, r);
        g_norm_p = zeros(1, r);
        
        cumulants = zeros(d, 1);
        moments = zeros(d, 1);

        mumuTaug = mu * muT + aug^2;

        MuISigmaJ = zeros(n, d);
        MuJSigmaI = zeros(n, d);

        if SharedCovariance
            SigmaI = Sigma;
            SigmaJ = Sigma;
            if d>=6
                [UI, S, ~] = svd(CholSigma);
                S = diag(S).^2;
                CSUI = CholSigma'*UI;
                CSUJ = CSUI;
            elseif d>=4
                SigmaISigmaJ = Sigma * Sigma;
            end
        end

        for i=1:r
            MuISigmaJ(:, 1) = muT(:, i);
            if ~SharedCovariance; SigmaI = Sigma(:,:,i); end
            for j=i:r
                MuJSigmaI(:, 1) = muT(:, j);
                if ~SharedCovariance; SigmaJ = Sigma(:,:,j); end
                for k=1:d/2
                    MuISigmaJ(:, 2*k) = SigmaJ *MuISigmaJ(:, 2*k-1);
                    MuJSigmaI(:, 2*k) = SigmaI *MuJSigmaI(:, 2*k-1);
                    if 2*k+1 <= d
                        MuISigmaJ(:, 2*k+1) = SigmaI *MuISigmaJ(:, 2*k);
                        MuJSigmaI(:, 2*k+1) = SigmaJ *MuJSigmaI(:, 2*k);
                    end
                end
                if SharedCovariance
                elseif d>=6
                    [UI, S, UJ] = svd(CholSigma(:, :, i)*CholSigma(:, :, j)');
                    S = diag(S);
                    CSUI = CholSigma(:, :, i)'*UI;
                    CSUJ = CholSigma(:, :, j)'*UJ;
                elseif d>=4
                    SigmaISigmaJ = SigmaI * SigmaJ;
                end

                cumulants(1) = mumuTaug(i,j);

                for k=1:d/2
                    % trace_part
                    if d>=6
                        trace_part = factorial_(2*k-1) * sum(S.^(2*k));
                    elseif k==1
                        trace_part = reshape(SigmaI,1,[]) * reshape(SigmaJ,[],1);
                    else
                        trace_part = 6*reshape(SigmaISigmaJ,1,[]) * reshape(SigmaISigmaJ',[],1);
                    end
                    %v_ij_part
                    cumulants(2*k) = trace_part + factorial_(2*k)*(MuISigmaJ(:, k)' * MuISigmaJ(:, k+1)+...
                                            MuJSigmaI(:, k)' * MuJSigmaI(:, k+1))/2;
                    if 2*k+1 <= d
                        cumulants(2*k+1) = factorial_(2*k+1) * MuISigmaJ(:, k+1)' * MuJSigmaI(:, k+1);
                    end
                end

                moments(1) = cumulants(1);
                for k=2:d
                    moments(k) = cumulants(k) + binom(k-1,1:k-1)*(...
                                    moments(1:k-1).*cumulants(k-1:-1:1));
                end
                
                g_norm_p(i) = g_norm_p(i) + 2*p(j)*moments(d);
                if j>i
                    g_norm_p(j) = g_norm_p(j) + 2*p(i)*moments(d);
                end
                
                g_norm_muI = g_norm_muT(:, i);
                if j>i; g_norm_muJ = g_norm_muT(:, j); end
                g_norm_SigmaI = zeros(n, n);
                if j>i; g_norm_SigmaJ = zeros(n, n); end
                
                for k=1:d
                    if k==d
                        alpha = pp(i,j)*factorial_(d);
                    else
                        alpha = pp(i,j)*binom(d,k)*factorial_(k)*moments(d-k);
                    end
                    if mod(k, 2)
                         g_norm_muI = g_norm_muI + alpha*MuJSigmaI(:, k);
                         g_norm_SigmaI = g_norm_SigmaI + ...
                             alpha * (MuJSigmaI(:, 1:2:k-2) * MuISigmaJ(:, k-1:-2:2)');
                         if j>i
                             g_norm_muJ = g_norm_muJ + alpha*MuISigmaJ(:, k);
                             g_norm_SigmaJ = g_norm_SigmaJ + ...
                             alpha * (MuISigmaJ(:, 1:2:k-2) * MuJSigmaI(:, k-1:-2:2)');
                         end
                    else
                         g_norm_muI = g_norm_muI + alpha*MuISigmaJ(:, k);
                         g_norm_SigmaI = g_norm_SigmaI + ...
                             (alpha/2) * (MuISigmaJ(:, 2:2:k-2) * MuISigmaJ(:, k-2:-2:2)');
                         g_norm_SigmaI = g_norm_SigmaI + ...
                             (alpha/2) * (MuJSigmaI(:, 1:2:k-1) * MuJSigmaI(:, k-1:-2:1)');
                         if j>i
                             g_norm_muJ = g_norm_muJ + alpha*MuJSigmaI(:, k);
                             g_norm_SigmaJ = g_norm_SigmaJ + ...
                                 (alpha/2) * (MuJSigmaI(:, 2:2:k-2) * MuJSigmaI(:, k-2:-2:2)');
                             g_norm_SigmaJ = g_norm_SigmaJ + ...
                                 (alpha/2) * (MuISigmaJ(:, 1:2:k-1) * MuISigmaJ(:, k-1:-2:1)');    
                         end    
                         if k==2
                             g_norm_SigmaI = g_norm_SigmaI + (alpha/2)*SigmaJ;
                             if j>i; g_norm_SigmaJ = g_norm_SigmaJ + (alpha/2)*SigmaI; end
                         elseif d>=6
                             g_norm_SigmaI = g_norm_SigmaI + (alpha/2)*...
                                 CSUJ*(S.^(k-2).*CSUJ');
                             if j>i; g_norm_SigmaJ = g_norm_SigmaJ + (alpha/2)*...
                                 CSUI*(S.^(k-2).*CSUI'); end
                         else %d<6 and k==4
                             g_norm_SigmaI = g_norm_SigmaI + (alpha/2)*...
                                 SigmaJ * SigmaISigmaJ;
                             if j>i; g_norm_SigmaJ = g_norm_SigmaJ + (alpha/2)*...
                                 SigmaISigmaJ * SigmaI; end
                         end
                    end
                end

                g_norm_muT(:, i) = g_norm_muI;
                if j>i; g_norm_muT(:, j) = g_norm_muJ; end
                if SharedCovariance
                    g_norm_sigma = g_norm_sigma + g_norm_SigmaI;
                    if j>i; g_norm_sigma = g_norm_sigma + g_norm_SigmaJ; end
                else
                    g_norm_sigma(:, :, i) = g_norm_sigma(:, :, i) + g_norm_SigmaI;
                    if j>i; g_norm_sigma(:, :, j) = g_norm_sigma(:, :, j) + g_norm_SigmaJ; end
                end                  
            end
        end

        f_norm = dot(g_norm_p, p) / 2;
        
    else
        cvt_diag = (CovarianceType == "diagonal");

        if cvt_diag
            n_ = n;
        else
            n_ = 1;
            mumuT = mu * muT;
            normmuT = sum(muT.^2);
        end
        
        % Get n x r Sigma and Mu
        if SharedCovariance
            matSigma = reshape(Sigma, n_, 1) + zeros(1, r);
        else
            matSigma = reshape(Sigma, n_, r);
        end
        
        moments = zeros(r, r, d);
        cumulants = zeros(r, r, d);
        
        for k=1:d
            if cvt_diag
                if k == 1
                    cumulantk = mu * muT + aug.^2;
                elseif mod(k, 2) == 1
                    SigmakMu = matSigma.^((k-1)/2).* muT;
                    cumulantk = factorial_(k) * (SigmakMu' * SigmakMu);
                else
                    Sigmak = matSigma.^(k/2);
                    trace_part = factorial_(k-1) * (Sigmak' * Sigmak);
                    % for the norm part, only do one side and then symmetrize
                    norm_part = (muT.^2 .* matSigma.^(k/2-1))' * matSigma.^(k/2);
                    norm_part = factorial_(k) / 2 * (norm_part + norm_part');
                    cumulantk = trace_part + norm_part;
                end
            else
                if k == 1
                    cumulantk = mumuT + aug.^2;
                elseif mod(k, 2) == 1
                    Sigmak1 = matSigma.^((k-1)/2);
                    cumulantk = factorial_(k) * (mumuT .* Sigmak1' .* Sigmak1);
                else
                    Sigmak = matSigma.^(k/2);
                    trace_part = n * factorial_(k-1) * (Sigmak' * Sigmak);
                    % for the norm part, only do one side and then symmetrize
                    norm_part = (normmuT .* matSigma.^(k/2-1))' * matSigma.^(k/2);
                    norm_part = factorial_(k) / 2 * (norm_part + norm_part');
                    cumulantk = trace_part + norm_part;
                end
            end
            cumulantk = (cumulantk + cumulantk') / 2;
            cumulants(:, :, k) = cumulantk;
            scalars = shiftdim(binom(k-1,1:k-1), -1);
            momentk = cumulantk + sum(scalars .* ...
                moments(:, :, 1:k-1) .* cumulants(:, :, k-1:-1:1),3);
            if ~issymmetric(momentk)
                warning('Bellk not symmetric')
            end
            moments(:, :, k) = momentk;
        end
        
        % Gradients
        g_norm_muT = 0;
        g_norm_sigma = 0;
        
        
        for k=1:d
            if k == d
                dQ = pp;
            else
                dQ = pp .* moments(:, :, d-k);
            end
            if mod(k, 2) == 1
                Sigmak = matSigma.^((k-1)/2);
                g_k_mu = (Sigmak .* muT) * dQ;
                g_k_mu = factorial_(k) * g_k_mu .* Sigmak;
                if k==1; g_k_sigma = 0;
                elseif cvt_diag
                    g_k_sigma = Sigmak * (mumuT .*  dQ);
                    g_k_sigma = factorial_(k) * (k-1) / 2 * ...
                        matSigma.^((k-3)/2) .* g_k_sigma;
                else
                    g_k_sigma = (matSigma.^((k-1)/2) .* muT) * dQ;
                    g_k_sigma = factorial_(k) * (k-1) / 2 * ...
                        matSigma.^((k-3)/2) .* muT .* g_k_sigma;
                end
            elseif cvt_diag
                Sigmak2dQ = matSigma.^(k/2) * dQ;
                Sigmak2m1 = matSigma.^(k/2-1);
                SdQSk2m1 = Sigmak2dQ .* Sigmak2m1;
                g_k_sigma_t = n * factorial_(k) / 2 * SdQSk2m1;
                g_k_mu = factorial_(k) * SdQSk2m1 .* muT;
        
                g_k_sigma_vij = (normmuT .* Sigmak2m1) * dQ;
                g_k_sigma_vij = (k / 2) * Sigmak2m1 .* g_k_sigma_vij;
                if k==2; g_k_sigma_vji=0;
                else
                    g_k_sigma_vji = (k/2 - 1) * normmuT .* matSigma.^(k/2-2) .* ...
                        Sigmak2dQ;
                end
                g_k_sigma_n = factorial_(k)/2 * (g_k_sigma_vij + g_k_sigma_vji);
                g_k_sigma = g_k_sigma_t + g_k_sigma_n;
            else
                Sigmak2dQ = matSigma.^(k/2) * dQ;
                Sigmak2m1 = matSigma.^(k/2-1);
                g_k_sigma_t = factorial_(k) / 2 * Sigmak2dQ .* Sigmak2m1;
                g_k_mu = 2 * g_k_sigma_t .* muT;
        
                g_k_sigma_vij = (muT.^2 .* Sigmak2m1) * dQ;
                g_k_sigma_vij = (k / 2) * Sigmak2m1 .* g_k_sigma_vij;
                if k==2; g_k_sigma_vji=0;
                else
                    g_k_sigma_vji = (k/2 - 1) * muT.^2 .* matSigma.^(k/2-2) .* ...
                        Sigmak2dQ;
                end
                g_k_sigma_n = factorial_(k)/2 * (g_k_sigma_vij + g_k_sigma_vji);
                g_k_sigma = g_k_sigma_t + g_k_sigma_n;
            end
        
            g_norm_muT = g_norm_muT + nchoosek(d, k) * g_k_mu;
            g_norm_sigma = g_norm_sigma + nchoosek(d, k) * g_k_sigma;
        end
        
        g_norm_sigma = reshape(g_norm_sigma, 1, n_, r);
        if SharedCovariance
            g_norm_sigma = sum(g_norm_sigma, 3);
        end
        
        g_norm_p = p * moments(:, :, d);
        f_norm = dot(g_norm_p, p);
        g_norm_p = 2 * g_norm_p;
    end

    g_norm_mu = g_norm_muT';
    
    f = fX + f_dot + f_norm;
    
    g_p = g_dot_p + g_norm_p ;
    g_mu = g_dot_mu + g_norm_mu;
    g_sigma = g_dot_sigma + g_norm_sigma;
    
    % Backprop Sigma gradient to CholSigma 
    switch CovarianceType
        case "full" 
          g_sigma = pagemtimes(CholSigma, ...
              g_sigma + permute(g_sigma, [2, 1, 3]));

          g_sigma(zero_out_indices) = 0;
        case {"isotropic", "spherical", "diagonal"}
          g_sigma = 2*g_sigma .* CholSigma;
    end
   
    
    if fixed_lambda
        g = [g_mu(:); g_sigma(:)];
    else 
        f = f + (sum(p) - 1)^2;
    
        g_p = g_p + 2*(sum(p) - 1);
    
        g = [g_mu(:); g_sigma(:); g_p(:)];
    end

    end

    function [f, g] = fg_moment_GMM_vecinput(X, v)

        [mu, CholSigma, p] = M_vec2params(v);
        if fixed_lambda % Return original p, since it is fixed
            p = Mp;
        end

        [f, g] = fg_moment_GMM_inner(X, mu, CholSigma, p);

    end

    function [out] = factorial_(n)
        out = reshape(vec_factorial(n+1), size(n));
    end

    function [out] = binom(n, k)
    % BINOM Vectorized binomial coefficient
    
    out = factorial_(n) ./ (factorial_(k) .* factorial_(n-k));

    end


end 

