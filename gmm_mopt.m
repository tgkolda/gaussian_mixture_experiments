function [M, info, M0, varargin] = gmm_mopt(X, r, nd, varargin)
%GMM_MOPT Fitting a GMM with implicit method of moments
%
%   M = GMM_MOPT(X, R, D) fits a GMMODEL using implicit method of
%    moments. The function requires
%     - X to be a N x n matrix where the rows are samples;
%     - R to be a positive integer number (number of mixture components);
%     - D to be a positive integer number (moment order);
%    and fits M by minimizing the function 
%
%      F(M) = ||Y-T||^2 = ||Y||^2 - 2 <T, Y> + ||T||^2
%
%   where Y is the empirical D-th moment tensor of the samples in X  
%   coming from a GMM and T is the D-th moment of the GMM defined by M. 
%   The terms <T, Y> and ||T||^2 are calculated implicitly using the 
%   formulas in [1].
%
%   [M, INFO] = GMM_MOPT(X,R) returns additional information in INFO.
%
%   [...] = GMM_MOPT(X, R, D, 'param','value') takes additional arguments:
%
%      'method' - Optimization algorithm. Default:'lbfgsb'. 
%         o 'lbfgsb' (Quasi-Newton method with bound constraints),
%         o 'lbfgs' (Quasi-Newton method from Poblano Toolbox), 
%         o 'fminunc'(Quasi-Netwon from Optimization Toolbox),
%         o 'adam' (stochastic gradient descent with momentum).              
%         For optimzation algorithm choices and parameters, see
%         <a href="matlab:web(strcat('file://',fullfile(getfield(what('tensor_toolbox'),'path'),'doc','html','opt_options_doc.html')))">Documentation for Tensor Toolbox Optimization Methods</a>
%
%      'init' - Initialization for factor matrix, Default: 'rrf'. 
%         The user can pass in an explicit initial guess as a symktenor or
%         a factor matrix, or pass in a function handle to generate the matrix
%         given the size as two arugments. Alternatively, the user can specify 
%         the following string options:
%              - 'rrf' for the randomized range finder with Gaussian weights.
%              - 'rrf_simplex' for the rrf with weights sampled from the simplex
%       
%      'preprocess' - If set to true, centers and rescales the samples.
%         It is set to false by default.
%
%      'augconst' - Augmentation constant, denoted by ω in [1]. See 
%         Section 5.2.
%      
%      'augmode' - Augmentation mode picked. Options are 'no' (no
%         augmentation), 'implicit' and 'post-process' (default). 
%         See section 6.3 in [1] for more details.
%
%      'CovarianceType' - Type of the covariances that GMM_MOPT shall fit.
%         Options are 'isotropic', 'diagonal' (default), and 'full' covariance
%         matrices.
%      
%      'SharedCovariance' - If set to True, fit the same covariance for all
%         mixture components, otherwise fit a covariance matrix for each
%         component. Default is 'false'.
%
%      'state' - Random state, to re-create the same outcome.
%
% [1] João M. Pereira, Joe Kileel, and Tamara G. Kolda. "Tensor Moments of 
%     Gaussian Mixture Models: Theory and Applications", arXiv:2202.06930 (2022). 
%     <a href="https://arxiv.org/abs/2202.06930">https://arxiv.org/abs/2202.06930</a>
%
%   <a href="matlab:web(strcat('file://',fullfile(getfield(what('tensor_toolbox'),'path'),'doc','html','cp_isym_doc.html')))">Additional documentation for CP-ISYM</a>
%
%   See also SYMKTENSOR/CP_ISYM.
%
% Tensor Toolbox for MATLAB: <a href="https://www.tensortoolbox.org">www.tensortoolbox.org</a>
%
% Code by Joao M. Pereira and Tamara Kolda, based on the code of cp_isym

%%
tic; % Start timer for setup costs - finished inside optimization call!

%% Random set-up
defaultStream = RandStream.getGlobalStream;

%% Parse Algorithm Parameters
[state, init, method, Xnormsqr, preprocess, augmode, ...
    c, fsamp, gsamp, printitn, CovType, ...
    SharedCov, optparams, params] = ...
        param_parser(varargin,...
                     {'state',     defaultStream.State},...
                     {'init',      'rrf_simplex'},...
                     {'method',    'lbfgsb'},...
                     {'Xnormsqr',  0},...
                     {'preprocess',false},...
                     {'augmode',   'default'},...
                     {'augconst',  1},...
                     {'fsamp',     1000},...
                     {'gsamp',     100},...
                     {'printitn',  1},...
                     {'CovarianceType',   'diagonal'},...
                     {'SharedCovariance', false});

optparams.printitn = printitn;

% Save
info.params = params;

%% Initialize random number generator with specified state
defaultStream.State = state;

%% Preprocess data
if preprocess
    n = size(X, 2);
    meanX = mean(X);
    X = X - meanX;
    if ismember(CovType, ["isotropic","spherical"])
        scaleX = sqrt(mean(X.^2, 'all') * n);
    else
        scaleX = sqrt(mean(X.^2) * n);
    end
    X = X ./ scaleX;
  end

%% Extract sizes, etc.
[N, n] = size(X);
if nargin < 3
    nd = 3;
end

%% Initial guess
if isa(init, 'gmdistribution')
    M0 = gmmodel(init);
else
    if isa(init,'function_handle')
        mu0 = init(r, n);
    elseif strcmpi(init,'rrf')
        mu0 = (randn(r, N) * X) / sqrt(N);
    elseif strcmpi(init,'rrf_simplex')
        sp = -log(rand(r, N));
        sp = sp ./ sum(sp, 2);
        mu0 = sp * X;
    elseif isequal(size(init),[r,n])
        if preprocess
            init = (init - meanX) ./ scaleX;
        end
        mu0 = init;
    else
        error('Invalid ''init'' option');
    end
    Sigma0 = mean(X.^2);
    if ismember(CovType, ["isotropic","spherical"])
        Sigma0 = mean(Sigma0);
    elseif CovType == "full"
        Sigma0 = diag(Sigma0);
    end
    if ~SharedCov
        Sigma0 = Sigma0 + zeros(1, 1, r);
    end
    lambda0 = ones(1, r) / r;
    M0 = gmmodel(mu0, Sigma0, lambda0);
end

assert(M0.checkparams(n, r, SharedCov, CovType),...
            'Initial guess has wrong parameters')

%% Compute ||X||^2
if ~isscalar(Xnormsqr)
    if isempty(Xnormsqr)
        Xnormsqr = 0;
    elseif strcmpi(Xnormsqr, 'moment')
        X_aug = X;
        if ismember(augmode, ["postprocess","implicit"])
            X_aug(:, end+1) = c;
        end 
        Xpow = khatri_rao_power(X_aug, floor(nd/2));
        if mod(nd,2)
            Xpow_ = khatri_rao_product(Xpow, X_aug);
        else
            Xpow_ = Xpow;
        end
        momX = (Xpow_'*Xpow) / size(X, 1);
        Xnormsqr = norm(momX(:))^2;
    elseif strcmpi(Xnormsqr, 'exact')
        error('Not implemented yet');
    elseif strcmpi(Xnormsqr,'random_subspace')
        error('Not implemented yet');
    end
end        

if augmode == "default"
    if SharedCov || ismember(CovType, ["isotropic","spherical"])
        augmode = 'implicit';
    else
        augmode = 'postprocess';
    end
end
        
switch augmode
    case {"no", "none","no augmentation"}
        warning("The no augmentation mode may give solutions " + ...
                "that are not scaled correctly");
        
        fghX = fg_moment_GMM(M0, X, nd, Xnormsqr, 0, false, true);

        mvec0 = tovec(M0, true);

        if method == "lbfgsb"
            optparams.lower = -Inf(size(mvec0));
            optparams.lower(end-r+1:end) = 0;
        end

    case "postprocess"
        if SharedCov || ismember(CovType, ["isotropic","spherical"])
            error('Not implemented yet')
        end
        
        % Augment X with
        X(:, end+1) = c;

        mu0 = [M0.mu, c * ones(r,1)];
        Sigma0 = M0.Sigma;
        Sigma0(:, end+1, :) = 0;
        if M0.CovarianceType == "full"
            Sigma0(end+1, :, :) = 0;
        end

        M0 = gmmodel(mu0, Sigma0, M0.ComponentProportion);
        
        fghX = fg_moment_GMM(M0, X, nd, Xnormsqr, 0, true, true, true);
        mvec0 = [M0.mu(:); M0.Sigma(:)];

    case "implicit"

        fghX = fg_moment_GMM(M0, X, nd, Xnormsqr, c, false, true);
        
        mvec0 = tovec(M0, true);

        if method == "lbfgsb"
            optparams.lower = -Inf(size(mvec0));
            optparams.lower(end-r+1:end) = 0;
        end
    
    otherwise
        error('Augmentation mode not implemented')
end



%% Finish setup
setuptime = toc; 

%% Optimization
tic
if (printitn > 0)
    fprintf('\nImplicit GMM Moment Optimization');
end

switch(method)
    case {'lbfgsb','lbfgs','fminunc'}
        fgh = @(mvec) fghX(X, mvec);
        %[f,g] = fgh(mvec0);
        %g_fd = finite_difference(fgh, mvec0, 1e-6);
        optname = sprintf('tt_opt_%s', method);
        [mvec,f,optinfo] = feval(optname, mvec0, fgh, optparams);

    case {'adam'}
        if strcmpi(fsamp,'exact')
            optparams.fexact = true;
            XF = X;
            optparams.fdesc = 'Function: exact';
        else
            optparams.fexact = false;
            XF = rand_extract(X, fsamp);
            if isscalar(fsamp)
                optparams.fdesc = sprintf('Function: %d samples out of %d observations', fsamp, ncomponents(X));
            else
                optparams.fdesc = sprintf('Function: %d user-specified samples out of %d observations', length(fsamp), ncomponents(X));
            end
        end
        fh = @(mvec) fghX(XF, mvec);
        gh = only_grad(@(mvec) fghX(rand_extract(X, gsamp), mvec));
        optparams.gdesc = sprintf('Gradient: using %d samples  out of %d observations', gsamp, ncomponents(X));       
        [mvec,f,optinfo] = tt_opt_adam(mvec0, fh, gh, optparams);
        
    otherwise
        error('Invalid method')
end
opttime = toc;

M = update(M0, mvec, true);

%% Clean up
if augmode == "postprocess"
    scale = M.mu(:, n + 1) / c;
    mu = M.mu(:, 1:n) ./ scale;
    scale = reshape(scale, 1, 1, r);
    if CovType == "full"
        CholSigma = M.CholSigma(1:n, 1:n, :) ./ abs(scale);
    else
        CholSigma = M.CholSigma(1, 1:n, :) ./ abs(scale);
    end
    scale = reshape(scale, 1, r);
    p = M.ComponentProportion .* (scale.^nd);
    p = max(p, 0);
    %sum(p)
    p = p ./ sum(p);
    M = gmmodel(mu, CholSigma, p, true);
else
%     p = M.ComponentProportion.^2;
%     p = p ./ sum(p);
%     M = gmmodel(M.mu, M.CholSigma, p, true);
end

if preprocess
    mu = M.mu .* scaleX + meanX;
    if CovType == "full"
        CholSigma = M.CholSigma .* scaleX';
    else
        CholSigma = M.CholSigma .* scaleX;
    end
    p = M.ComponentProportion;
    M = M.update(mu, CholSigma, p, true);
end

%% Save results

info.f = f;
info.optout = optinfo;
info.opttime = opttime;
info.setuptime = setuptime;
info.Xnormsqr = Xnormsqr;
info.opttime = opttime;
info.setuptime = setuptime;

end

%%
function Xs = randextract(X, m)
%RANDEXTRACT  

    n = size(X, 1);
    ind = sort(randperm(n, m));

    Xs = X(ind, :);

end

%%
function og = only_grad(fg)
%ONLY_GRAD Returns a function that only returns gradient

    function g = only_grad_inner(X)
        [~, g] = fg(X);
    end

    og = @only_grad_inner;
    
end

function varargout = param_parser(user_input, varargin)
%OPTION_PARSER Parse algorithm parameters and defaults
  
  n = length(varargin);
  iP = inputParser;
  iP.KeepUnmatched = true;
  iP.PartialMatching = false;
  % vargin are the default options
  for i=1:n
      addParameter(iP,varargin{i}{:});
  end
  parse(iP, user_input{:});
  
  varargout{n+1} = iP.Unmatched;
  varargout{n+2} = iP.Results;

  for i=1:n
      varargout{i} = iP.Results.(varargin{i}{1});
  end
  
end

function An = khatri_rao_power(A, n)

if n==0
    An = 1;
elseif n==1
    An = A;
elseif n==2
    An = khatri_rao_product(A, A);
else
    An2 = khatri_rao_power(A, floor(n/2));
    if mod(n,2)
        An2_ = khatri_rao_product(An2, A);
    else
        An2_ = An2;
    end
    An = khatri_rao_product(An2, An2_);
end
end

function AB = khatri_rao_product(A, B)

n = size(A, 1);
AB = reshape(A .* reshape(B, n, 1, []), n, []);

end
