function rel_err = moment_error(Mtrue, Mest, d, aug)

if nargin < 4; aug = 0; end

vec_factorial = ones(d+1,1);

for k_ = 2:d
    vec_factorial(k_ + 1) = factorial(k_);
end

Mtrue_norm = moment_dot(Mtrue, Mtrue, d, aug);

err = Mtrue_norm + moment_dot(Mest, Mest, d, aug) - ...
                   2 * moment_dot(Mtrue, Mest, d, aug);

rel_err = err / Mtrue_norm;



    function mdot = moment_dot(M1, M2, d, aug)
        
        [M1, M2] = broadcast(M1, M2);
    
        SharedCovariance = M1.SharedCovariance;
        CovarianceType = M1.CovarianceType;
    
        CholSigmaIs = M1.CholSigma;
        CholSigmaJs = M2.CholSigma;
        SigmaIs = M1.Sigma;
        SigmaJs = M2.Sigma;
    
        muTI = M1.mu';
        muTJ = M2.mu';
        [n, rI] = size(muTI);
        [~, rJ] = size(muTJ);

        pI = M1.PComponents;
        pJ = M2.PComponents;
    
        if CovarianceType == "full"
    
            mdot = 0;
            
            cumulants = zeros(d, 1);
            moments = zeros(d, 1);
    
            mumuTaug = muTI' * muTJ + aug^2;
    
            d2 = floor(d/2) + 1;
            MuISigmaJ = zeros(n, d2);
            MuJSigmaI = zeros(n, d2);
    
            if SharedCovariance
                SigmaI = SigmaIs;
                SigmaJ = SigmaJs;
                if d>=6
                    S = svd(CholSigmaIs * CholSigmaJs');
                elseif d>=4
                    SigmaISigmaJ = SigmaI * SigmaJ;
                end
            end
    
            for i=1:rI
                MuISigmaJ(:, 1) = muTI(:, i);
                if ~SharedCovariance; SigmaI = SigmaIs(:,:,i); end
                for j=1:rJ
                    MuJSigmaI(:, 1) = muTJ(:, j);
                    if ~SharedCovariance; SigmaJ = SigmaJs(:,:,j); end
                    for k=1:d2/2
                        MuISigmaJ(:, 2*k) = SigmaJ *MuISigmaJ(:, 2*k-1);
                        MuJSigmaI(:, 2*k) = SigmaI *MuJSigmaI(:, 2*k-1);
                        if 2*k+1 <= d2
                            MuISigmaJ(:, 2*k+1) = SigmaI *MuISigmaJ(:, 2*k);
                            MuJSigmaI(:, 2*k+1) = SigmaJ *MuJSigmaI(:, 2*k);
                        end
                    end
                    if SharedCovariance
                    elseif d>=6
                        S = svd(CholSigmaIs(:, :, i)*CholSigmaJs(:, :, j)');
                    elseif d>=4
                        SigmaISigmaJ = SigmaI * SigmaJ;
                    end
    
                    cumulants(1) = mumuTaug(i,j);
    
                    for k=1:d/2
                        % trace_part
                        if d>=6
                            trace_part = factorial_(2*k-1) * sum(S.^(2*k));
                        elseif k==1
                            trace_part = reshape(SigmaI,1,[]) * reshape(SigmaJ,[],1);
                        else
                            trace_part = 6*reshape(SigmaISigmaJ,1,[]) * reshape(SigmaISigmaJ',[],1);
                        end
                        %v_ij_part
                        cumulants(2*k) = trace_part + factorial_(2*k)*(MuISigmaJ(:, k)' * MuISigmaJ(:, k+1)+...
                                                MuJSigmaI(:, k)' * MuJSigmaI(:, k+1))/2;
                        if 2*k+1 <= d
                            cumulants(2*k+1) = factorial_(2*k+1) * MuISigmaJ(:, k+1)' * MuJSigmaI(:, k+1);
                        end
                    end
    
                    moments(1) = cumulants(1);
                    for k=2:d
                        moments(k) = cumulants(k) + combs(k-1,1:k-1)*(...
                                        moments(1:k-1).*cumulants(k-1:-1:1));
                    end

                    mdot = mdot + pI(i) * pJ(j) * moments(d);
                    
                end
            end
            
        else
            cvt_diag = (CovarianceType == "diagonal");
    
            if cvt_diag
                n_ = n;
            else
                n_ = 1;
                mumuT = muTI' * muTJ;
                normmuTI = sum(muTI.^2);
                normmuTJ = sum(muTJ.^2);
            end
            
            % Get n x r Sigma and Mu
            if SharedCovariance
                matSigmaI = reshape(SigmaIs, n_, 1) + zeros(1, rI);
                matSigmaJ = reshape(SigmaJs, n_, 1) + zeros(1, rJ);
            else
                matSigmaI = reshape(SigmaIs, n_, rI);
                matSigmaJ = reshape(SigmaJs, n_, rJ);    
            end
            

            Bell = zeros(rI, rJ, d-1);
            Kappa = zeros(rI, rJ, d-1);
            
            for k=1:d
                if cvt_diag
                    if k == 1
                      Kappak = muTI' * muTJ + aug.^2;
                    elseif mod(k, 2) == 1
                      SigmakMuI = matSigmaI.^((k-1)/2).* muTI;
                      SigmakMuJ = matSigmaJ.^((k-1)/2).* muTJ;
                      Kappak = factorial_(k) * (SigmakMuI' * SigmakMuJ);
                    else
                      trace_part = factorial_(k-1) * (matSigmaI'.^(k/2) * matSigmaJ.^(k/2));
                      norm_part1 = (muTI.^2 .* matSigmaI.^(k/2-1))' * matSigmaJ.^(k/2);
                      norm_part2 = matSigmaI'.^(k/2) * (muTJ.^2 .* matSigmaJ.^(k/2-1));
                      norm_part = factorial_(k) / 2 * (norm_part1 + norm_part2);
                      Kappak = trace_part + norm_part;              
                    end
                else
                    if k == 1
                        Kappak = mumuT + aug.^2;
                    elseif mod(k, 2) == 1
                        SigmakI = matSigmaI.^((k-1)/2);
                        SigmakJ = matSigmaJ.^((k-1)/2);
                        Kappak = factorial_(k) * (mumuT .* SigmakI' .* SigmakJ);
                    else
                        trace_part = n * factorial_(k-1) * (matSigmaI'.^(k/2) * matSigmaJ.^(k/2));
                        norm_part1 = (normmuTI .* matSigmaI.^(k/2-1))' * matSigmaJ.^(k/2);
                        norm_part2 = matSigmaI'.^(k/2) .* (normmuTJ .* matSigmaJ.^(k/2-1));
                        norm_part = factorial_(k) / 2 * (norm_part1 + norm_part2);
                        Kappak = trace_part + norm_part;
                    end
                end
                scalars = shiftdim(combs(k-1,1:k-1), -1);
                Bellk = Kappak + sum(scalars .* ...
                  Bell(:, :, 1:k-1) .* Kappa(:, :, k-1:-1:1),3);
                if k<d
                  Kappa(:, :, k) = Kappak;
                  Bell(:, :, k) = Bellk;
                else
                  Belld = Bellk;
                end
            end
            
            mdot = pI * Belld * pJ';
        end
    
        

    end

    function [out] = factorial_(n)
        out = reshape(vec_factorial(n+1), size(n));
    end

    
    function [out] = combs(n, k)
    % BINOM Vectorized binomial coefficient
    
    out = factorial_(n) ./ (factorial_(k) .* factorial_(n-k));

    end

end