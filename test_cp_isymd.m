%%
randstate = randi(1e9);
fprintf('randstate=%d\n',randstate);
rng(randstate);

n = 500;
m_true = 10;
m = m_true;
cong = 0.5;
sigma = 1e-1;

%[mu_true,w_true,gmd] = debias_experiments_setup(n,m,sigma^2,'cong',cong,'minwgt',0.05,'lmean',1,'lstd',0.2);
[mu_true,w_true,gmd] = debias_experiments_setup(n,m,sigma^2,'cong',cong);

r = m_true*250; % number of realizations/observations
X = transpose(random(gmd,r));
Sigma = sigma^2*speye(n);

d=3;
K_true = symktensor(w_true, mu_true, d);


%%

randstate = randi(1e9);
fprintf('randstate=%d\n',randstate);
rng(randstate);

d=3;
c = 0.1;
opts = struct();
opts.printitn = 1000;
opts.maxiters = 5000;
opts.ftol=1e-14;

XX = symktensor(1/r*ones(r,1),X,d);
K4 = cp_isymd(XX,m,Sigma,opts,'augmented',c);
foo = debias_eval_soln(w_true,mu_true,K4.lambda,K4.u,Sigma,X,d)

[w_true(foo.matching(:,1)) K4.lambda(foo.matching(:,2))]
