%%
n = 100; % Problem dimension
d = 4;
m = 10; % number of clusters
r = 1000; % number of samples

%mu = randn(m, n) / sqrt(n);
A = matrandcong(n,m,0.5);
mu = A'/sqrt(n);

Sigma = 20*(1+rand(1,n,m)) / n;

%%
% To sample uniformly from simplex
% Sample independent r.v's with exponential
% distribution and divide by sum
% To sample exponential do -log(rand)
p = -log(rand(1, m));
p = .2 / m + .8 * (p ./ sum(p));

M = gmmodel(mu, Sigma, p);
X = M.random(r);

optopts = {'CovarianceType', 'diagonal'};
optopts = [optopts, {'printitn', 0}];
% optopts = [optopts, {'method', 'fminunc'}];
% optopts = [optopts, {'CheckGradients', true}];
% optopts = [optopts, {'FiniteDifferenceStepSize', 1e-6}];
% optopts = [optopts, {'FiniteDifferenceType', 'central'}];
optopts = [optopts, {'augmode', 'implicit'}];

M_est = gmm_mopt(X, m, d, optopts{:});

moment_error(M, M_est, d, .1)
sum(M_est.log_pdf(X)-M.log_pdf(X))

optopts = {'CovarianceType', 'diagonal'};
optopts = [optopts, {'printitn', 0}];

M_est = gmm_mopt(X, m, d, optopts{:});

moment_error(M, M_est, d, .1)
sum(M_est.log_pdf(X)-M.log_pdf(X))