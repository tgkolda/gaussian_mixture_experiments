%% Set up GMM
n = 2;
m = 3;

mu = 2*[
    0.5, -0.5; 
    0.5 0.5; 
    -0.5 0.5]; % mu(i,:) is the ith mean

%sigmasqr = 0.4;
%sigma = reshape(sigmasqr * ones(2,3), [1 2 3]);  % sigma(1,:,i) is the diag of the ith covariance
Sigma = [0.4 0.2; 0.2 0.3];
lambda = [0.4, 0.3 0.3];

gm = gmdistribution(mu,Sigma,lambda);


xmin = 2*-1.3;
xmax = 2*1.3;
ymin = 2*-1.3;
ymax = 2*1.3;

% Plot surface and contour plots
npts = 70;
x = linspace(xmin,xmax,npts);
y = linspace(ymin,ymax,npts);
[xx,yy] = meshgrid(x,y);
zz = pdf(gm,[xx(:),yy(:)]);
zz = reshape(zz,npts,npts);
figure(2); clf;
contour(xx,yy,zz,20);
exportgraphics(gca,'twodim_debias_validate_2d.png','Resolution',300)

figure(3); clf;
surf(xx,yy,zz);
exportgraphics(gca,'twodim_debias_validate_3d.png','Resolution',300)
%% Debias moments
A = mu';
Ya = symktensor(lambda',A,3);
Yb = full(Ya);



%% Create data
rvals = floor(10.^(1:7));
%Sigma = sigmasqr*eye(n);
%%
rng('default')
Xall = random(gm,rvals(end));
diff = zeros(length(rvals),1);
for i = 1:length(rvals)
    r = rvals(i);

    X = Xall(1:r,:);
    
    % Third-moment debias
    xxx = khatrirao(X',X',X');
    xs_ = ttt(tensor(Sigma),tensor(X'));
    xs = reshape(double(xs_),8,r);
    Za = sum(xxx-3*xs,2)/r;
    Zb = tensor(Za,[2 2 2]);
    Zc = symmetrize(Zb);
    Zd = symtensor(Zc);
    
    diff(i) = norm( tensor(Yb) - tensor(Zd) );
end
diff

%%
C = [228,26,28
55,126,184
77,175,74
152,78,163
255,127,0]/255;

figure(1); clf;
loglog(rvals,diff,'-o','LineWidth',2,'Color',C(1,:),'MarkerFaceColor',C(1,:))
%xlabel('Number of Realizations (r)')
%ylabel('Norm of Difference')
width = 4.5;     % Width in inches
height = 2.5;    % Height in inches
pos = get(gcf, 'Position');
delta_y = max(0, height*100 - pos(4));
set(gcf, 'Position', [pos(1) (pos(2)-delta_y) width*100, height*100]); 
exportgraphics(gca,'..\gaussian_mixtures_paper\img\twodim_debias_validate.png','Resolution',300)
